package jp.alhinc.yamauchi_kenichiro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {

		Map<String, String> map1 = new HashMap<>();
		Map<String, Long> map2 = new HashMap<>();
		try {

			BufferedReader br = null;
			try {
				File branchLst = new File(args[0], "branch.lst");

				if (!branchLst.exists()) {
					System.out.println("支店定義ファイルが存在しません");
					return;
				}

				br = new BufferedReader(new FileReader(branchLst));

				String s;
				while ((s = br.readLine()) != null) {
					String[] array = s.split(",");
					if (array.length != 2 || !array[0].matches("[0-9]{3}") || array[1].isEmpty()) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}

					map1.put(array[0], array[1]);
					map2.put(array[0], 0L);
				}
			} finally {
				if (br != null) {
					br.close();
				}
			}

			File directory = new File(args[0]);
			String[] filenames = directory.list();

			List<Integer> rcdNumbers = new ArrayList<>();
			for (String filename : filenames) {
				if (!filename.matches("[0-9]{8}.rcd")) {
					continue;
				}

				rcdNumbers.add(Integer.parseInt(filename.substring(0, 8)));

				try {
					File rcdFile = new File(args[0], filename);
					br = new BufferedReader(new FileReader(rcdFile));
					String code = br.readLine();
					long amount = Long.parseLong(br.readLine());
					if (br.readLine() != null) {
						System.out.println(filename + "のフォーマットが不正です");
						return;
					}

					if (!map1.containsKey(code)) {
						System.out.println(filename + "の支店コードが不正です");
						return;
					}

					map2.put(code, map2.get(code) + amount);
					if (map2.get(code).toString().length() > 10) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
				} finally {
					br.close();
				}
			}

			Collections.sort(rcdNumbers);
			int min = rcdNumbers.get(0);
			int max = rcdNumbers.get(rcdNumbers.size() - 1);
			if (min + rcdNumbers.size() - 1 != max) {
				System.out.println("ファイル名が連番になっていません");
				return;
			}

			File branchOut = new File(args[0], "branch.out");
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(branchOut));
				for (String key : map1.keySet()) {
					bw.write(key + "," + map1.get(key) + "," + map2.get(key));
					bw.newLine();
				}
			} finally {
				if (bw != null) {
					bw.close();
				}
			}

		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました");
		}
	}
}